~~Requisitos

1. SWI-Prolog <http://www.swi-prolog.org/>
2. Java SE 6 <http://www.oracle.com/technetwork/java/javase/downloads/java-archive-downloads-javase6-419409.html>

--------------------------------------------------------------------

~~Config Variáveis de Ambiente

Adicione à variável PATH os caminhos para a JPL Library (jpl.jar), 
para a DLL da JPL (jpl.dll) e para o Java 6 (JDK 1.6);

--------------------------------------------------------------------

~~Referências

http://www.cachorroideal.com

http://tudosobrecachorros.com.br/guia-de-racas-caes-completo

http://www.happyharbor.com.br

http://www.berneseecia.com

http://www.petbrazil.com.br

http://www.saudeanimal.com.br

http://www.cachorrogato.com.br

http://www.kennelclub.com.br

http://www.pedigree.com.br/meu-amigo-ideal/

http://mdemulher.abril.com.br/casa/testes/bichos/qual-melhor-cachorro-voce-399286.shtml

http://www.swi-prolog.org/packages/jpl/

--------------------------------------------------------------------

~~Créditos aos Autor do Ícone

Icon made by Freepik
<http://www.freepik.com> 
from Flaticons
<http://www.flaticon.com> 
is licensed under Creative Commons BY 3.0 (CC BY 3.0)
<http://creativecommons.org/licenses/by/3.0/>

