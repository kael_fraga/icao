/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifrs.canoas.lpoo2.gui;

import br.edu.ifrs.canoas.lpoo2.control.RacaIdeal;
import javax.swing.ButtonModel;
import javax.swing.JOptionPane;

/**
 *
 * @author Kael
 */
public class Pergunta8 extends javax.swing.JFrame {

    /**
     * Creates new form Pergunta8
     */
    public Pergunta8() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        btGroupPerguntas = new javax.swing.ButtonGroup();
        pnHeader = new javax.swing.JPanel();
        lbHeader = new javax.swing.JLabel();
        lbPergunta = new javax.swing.JLabel();
        rbPossuiCaes = new javax.swing.JRadioButton();
        rbNaoPossui = new javax.swing.JRadioButton();
        btAvancar = new javax.swing.JButton();
        btVoltar = new javax.swing.JButton();
        lbPergunta2 = new javax.swing.JLabel();
        lbPergunta1 = new javax.swing.JLabel();
        rbPossuiAnimais = new javax.swing.JRadioButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("ICão");
        setBackground(new java.awt.Color(189, 195, 199));
        setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        setMinimumSize(new java.awt.Dimension(800, 600));
        setResizable(false);

        pnHeader.setBackground(new java.awt.Color(231, 76, 60));

        lbHeader.setFont(new java.awt.Font("Tw Cen MT", 1, 30)); // NOI18N
        lbHeader.setForeground(new java.awt.Color(189, 195, 199));
        lbHeader.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        lbHeader.setIcon(new javax.swing.ImageIcon(getClass().getResource("/br/edu/ifrs/canoas/lpoo2/assets/dogo.png"))); // NOI18N
        lbHeader.setText("iCão");
        lbHeader.setToolTipText("");
        lbHeader.setVerticalAlignment(javax.swing.SwingConstants.TOP);

        javax.swing.GroupLayout pnHeaderLayout = new javax.swing.GroupLayout(pnHeader);
        pnHeader.setLayout(pnHeaderLayout);
        pnHeaderLayout.setHorizontalGroup(
            pnHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lbHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        pnHeaderLayout.setVerticalGroup(
            pnHeaderLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(pnHeaderLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbHeader, javax.swing.GroupLayout.DEFAULT_SIZE, 46, Short.MAX_VALUE))
        );

        lbPergunta.setFont(new java.awt.Font("Tw Cen MT", 0, 30)); // NOI18N
        lbPergunta.setText("8. Você já possui cães em casa, ou ");

        btGroupPerguntas.add(rbPossuiCaes);
        rbPossuiCaes.setFont(new java.awt.Font("Tw Cen MT", 0, 24)); // NOI18N
        rbPossuiCaes.setText("Sim, possuo outros cães.");
        rbPossuiCaes.setToolTipText("");
        rbPossuiCaes.setActionCommand("com_outros");
        rbPossuiCaes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPossuiCaesActionPerformed(evt);
            }
        });

        btGroupPerguntas.add(rbNaoPossui);
        rbNaoPossui.setFont(new java.awt.Font("Tw Cen MT", 0, 24)); // NOI18N
        rbNaoPossui.setText("Não.");
        rbNaoPossui.setToolTipText("");
        rbNaoPossui.setActionCommand("sem_outros");
        rbNaoPossui.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbNaoPossuiActionPerformed(evt);
            }
        });

        btAvancar.setBackground(new java.awt.Color(189, 195, 199));
        btAvancar.setFont(new java.awt.Font("Tw Cen MT", 0, 24)); // NOI18N
        btAvancar.setText("Avançar");
        btAvancar.setToolTipText("");
        btAvancar.setMaximumSize(new java.awt.Dimension(140, 70));
        btAvancar.setMinimumSize(new java.awt.Dimension(140, 65));
        btAvancar.setPreferredSize(new java.awt.Dimension(140, 65));
        btAvancar.setRolloverEnabled(false);
        btAvancar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btAvancarActionPerformed(evt);
            }
        });

        btVoltar.setBackground(new java.awt.Color(189, 195, 199));
        btVoltar.setFont(new java.awt.Font("Tw Cen MT", 0, 24)); // NOI18N
        btVoltar.setText("Voltar");
        btVoltar.setToolTipText("");
        btVoltar.setMaximumSize(new java.awt.Dimension(140, 70));
        btVoltar.setMinimumSize(new java.awt.Dimension(140, 65));
        btVoltar.setPreferredSize(new java.awt.Dimension(140, 65));
        btVoltar.setRolloverEnabled(false);
        btVoltar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btVoltarActionPerformed(evt);
            }
        });

        lbPergunta2.setFont(new java.awt.Font("Tw Cen MT", 0, 30)); // NOI18N
        lbPergunta2.setText("8 de 9");

        lbPergunta1.setFont(new java.awt.Font("Tw Cen MT", 0, 30)); // NOI18N
        lbPergunta1.setText("outros animais?");

        btGroupPerguntas.add(rbPossuiAnimais);
        rbPossuiAnimais.setFont(new java.awt.Font("Tw Cen MT", 0, 24)); // NOI18N
        rbPossuiAnimais.setText("Sim, outros animais. ");
        rbPossuiAnimais.setToolTipText("");
        rbPossuiAnimais.setActionCommand("com_outros");
        rbPossuiAnimais.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbPossuiAnimaisActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(pnHeader, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(150, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbNaoPossui)
                    .addComponent(lbPergunta1)
                    .addComponent(lbPergunta)
                    .addComponent(rbPossuiCaes)
                    .addComponent(rbPossuiAnimais)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(btVoltar, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(65, 65, 65)
                        .addComponent(lbPergunta2, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(64, 64, 64)
                        .addComponent(btAvancar, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(151, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(pnHeader, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 50, Short.MAX_VALUE)
                .addComponent(lbPergunta)
                .addGap(0, 3, Short.MAX_VALUE)
                .addComponent(lbPergunta1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addComponent(rbPossuiCaes, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15)
                .addComponent(rbPossuiAnimais)
                .addGap(15, 15, 15)
                .addComponent(rbNaoPossui)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 82, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btAvancar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btVoltar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(lbPergunta2))
                .addContainerGap(100, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbPossuiCaesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPossuiCaesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbPossuiCaesActionPerformed

    private void rbNaoPossuiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbNaoPossuiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbNaoPossuiActionPerformed

    private void btAvancarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btAvancarActionPerformed
        ButtonModel btModel = btGroupPerguntas.getSelection();
        if (btModel != null) {
            RacaIdeal.setPresencaOutrosAnimais(btModel.getActionCommand());
            this.dispose();
            new Pergunta9().setVisible(true);
        } else {
            JOptionPane.showMessageDialog(this, "ESCOLHA UMA DAS ALTERNATIVAS!", null, JOptionPane.WARNING_MESSAGE, null);
        }
    }//GEN-LAST:event_btAvancarActionPerformed

    private void btVoltarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btVoltarActionPerformed
        this.dispose();
        new Pergunta7().setVisible(true);
    }//GEN-LAST:event_btVoltarActionPerformed

    private void rbPossuiAnimaisActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbPossuiAnimaisActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbPossuiAnimaisActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pergunta8.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pergunta8.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pergunta8.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pergunta8.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Pergunta8().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btAvancar;
    private javax.swing.ButtonGroup btGroupPerguntas;
    private javax.swing.JButton btVoltar;
    private javax.swing.JLabel lbHeader;
    private javax.swing.JLabel lbPergunta;
    private javax.swing.JLabel lbPergunta1;
    private javax.swing.JLabel lbPergunta2;
    private javax.swing.JPanel pnHeader;
    private javax.swing.JRadioButton rbNaoPossui;
    private javax.swing.JRadioButton rbPossuiAnimais;
    private javax.swing.JRadioButton rbPossuiCaes;
    // End of variables declaration//GEN-END:variables
}
