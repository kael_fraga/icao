/*
 * To change RacaIdeal license header, choose License Headers in Project Properties.
 * To change RacaIdeal template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifrs.canoas.lpoo2.control;

import java.util.Hashtable;
import java.util.LinkedList;

/**
 *
 * @author Kael
 */
public class RacaIdeal {
    /*Atributos
     espacoNecessario: apt, casa
     qtdExercicio: pouco, muito
     temperamento: tranquilo, ativo 
     pelo: longo, curto
     funcao: guarda, companhia
     ambiente: casa_vazia, casa_cheia
     preco: 0-1000000
     outros_animais: com_outros, sem_outros
     clima: frio, calor, ambos
     */

    private static String espacoNecessario;
    private static String qtdExercicio;
    private static String temperamento;
    private static String pelo;
    private static String funcao;
    private static String ambiente;
    private static long preco;
    private static String presencaOutrosAnimais;
    private static String clima;

    static {
        RacaIdeal.espacoNecessario = "";
        RacaIdeal.qtdExercicio = "";
        RacaIdeal.temperamento = "";
        RacaIdeal.pelo = "";
        RacaIdeal.funcao = "";
        RacaIdeal.ambiente = "";
        RacaIdeal.preco = 0;
        RacaIdeal.presencaOutrosAnimais = "";
        RacaIdeal.clima = "";
    }

    public static void setEspacoNecessario(String espacoNecessario) {
        RacaIdeal.espacoNecessario = espacoNecessario;
    }

    public static void setQtdExercicio(String qtdExercicio) {
        RacaIdeal.qtdExercicio = qtdExercicio;
    }

    public static void setTemperamento(String temperamento) {
        RacaIdeal.temperamento = temperamento;
    }

    public static void setPelo(String pelo) {
        RacaIdeal.pelo = pelo;
    }

    public static void setFuncao(String funcao) {
        RacaIdeal.funcao = funcao;
    }

    public static void setAmbiente(String ambiente) {
        RacaIdeal.ambiente = ambiente;
    }

    public static void setPreco(long preco) {
        RacaIdeal.preco = preco;
    }

    public static void setPresencaOutrosAnimais(String presencaOutrosAnimais) {
        RacaIdeal.presencaOutrosAnimais = presencaOutrosAnimais;
    }

    public static void setClima(String clima) {
        RacaIdeal.clima = clima;
    }

    public static LinkedList<String> getRacaIdeal(){ 
        LinkedList<String> results;
        String str = "raca_ideal(X,["
                + espacoNecessario + ","
                + qtdExercicio + ","
                + temperamento + ","
                + pelo + ","
                + funcao + ","
                + ambiente + ","
                + preco + ","
                + presencaOutrosAnimais + ","
                + clima + "]).";
        
        System.out.println("Query = " + str);
        results = KnowledgeBase.executeQuery(str);

        if (results.size() == 0) {
            results.add("gato");
        }           
        return results;
    }
    
    public static String getDescricaoCachorro(String nome_cachorro){
        Filesystem fs = new Filesystem();
        
        String descricao = fs.read("src/descricoes/"+nome_cachorro+".txt");
        
        if(descricao == null){
            return nome_cachorro;
        }else{
            return descricao;
        }            
    }
}
