/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifrs.canoas.lpoo2.control;

import java.io.*;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author kf27005
 */
public class Filesystem {

    public Filesystem() {
    }

    public String read(String fileName) {

        BufferedReader br;
        String str = null;

        try {

            System.out.println(fileName);

            br = new BufferedReader(new FileReader(fileName));

            str = br.readLine();

            br.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        return str;
    }

    public String choiseFile(String message, String extension) {
        JFileChooser chooser = new JFileChooser();

        FileNameExtensionFilter filter = new FileNameExtensionFilter(
                message, extension);

        chooser.setFileFilter(filter);

        int returnVal = chooser.showOpenDialog(null);

        if (returnVal == JFileChooser.APPROVE_OPTION) {

            File file = chooser.getSelectedFile();

            System.out.println("You chose to open this file: "
                    + file.getName());

            return file.getAbsolutePath();
        }
        
        return null;
    }
}
