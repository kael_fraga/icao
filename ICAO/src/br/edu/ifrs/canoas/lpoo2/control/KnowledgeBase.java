/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.ifrs.canoas.lpoo2.control;

import java.io.File;
import java.util.Hashtable;
import java.util.LinkedList;
import jpl.*;
import static javax.swing.JOptionPane.*;

/**
 *
 * @author Kael
 */
public class KnowledgeBase {

    public static boolean initializeBase(String prolog_file_name) {
        try {
            Filesystem fl = new Filesystem();
            //String base_path = fl.choiseFile("Knowledge Base", "pl");
            //if(base_path == null)
            //    return false;           
            // System.out.println(base_path);
            System.out.println(prolog_file_name);
            File base = new File(prolog_file_name);
            String base_path =  base.getAbsolutePath();
            System.out.println(base_path);
            base_path = base_path.replace('\\', '/');
            System.out.println(base_path);
            String kbase = "consult('" + base_path + "').";
            System.out.println(kbase);
            Query query = new Query(kbase);
            return query.hasSolution();
        } catch (Exception e) {
            showMessageDialog(null, "Erro! Problema na biblioteca JPL.", null, ERROR_MESSAGE);
            e.printStackTrace();
        }
        return false;
    }

    public static LinkedList<String> executeQuery(String strQuery) {

        LinkedList<String> results = new LinkedList<String>();
        Hashtable htab;

        try {
            Query query = new Query(strQuery);

            while (query.hasMoreSolutions()) {
                htab = query.nextSolution();
                System.out.println("Raça ideal = " + htab.get("X").toString());
                results.add(htab.get("X").toString());
            }
            return results;

        } catch (Exception e) {
            showMessageDialog(null, "Erro! Problema na consulta.", null, ERROR_MESSAGE);
            e.printStackTrace();
        }
        return results;
    }
}
