﻿%raca_ideal(X,[casa,muito,tranquilo,longo,companhia,casa_cheia,3000,sem_outros,frio]).

/*Atributos
espaco: apt, casa(não elimina)
exercicio: pouco, muito(não elimina)
temperamento: tranquilo, ativo 
pelo: longo, curto
funcao: guarda, companhia
sozinho: casa_vazia, casa_cheia(não elimina)
preco: 0-1000000(não elimina)
outros_animais: com_outros, sem_outros(não elimina)
clima: frio, calor, ambos

*/

%Espaco
espaco(pastor,casa).
espaco(golden,casa).
espaco(collie,casa).
espaco(labrador,casa).
espaco(yorkshire,_).
espaco(husky,casa).
espaco(pug,_).
espaco(fila,casa).
espaco(lhasa,_).
espaco(bulldog,_).
espaco(chihuahua,_).
espaco(akita,casa).
espaco(basset,casa).
espaco(beagle,casa).
espaco(dachshund,_).
espaco(poodle,casa).
espaco(pinscher,_).
espaco(rottweiler,casa).
espaco(bernardo,casa).
espaco(chow,casa).
espaco(xoloitzcuintli,_).
espaco(pitbull,_).

%Necessidade de Exercícios
exercicio(rottweiler,muito).
exercicio(bernardo,muito).
exercicio(chow,muito).
exercicio(pinscher,muito).
exercicio(poodle,_).
exercicio(dachshund,_).
exercicio(beagle,muito).
exercicio(basset,muito).
exercicio(akita,muito).
exercicio(chihuahua,_).
exercicio(bulldog,_).
exercicio(lhasa,_).
exercicio(fila,muito).
exercicio(husky,muito).
exercicio(pug,_).
exercicio(yorkshire,_).
exercicio(labrador,muito).
exercicio(collie,_).
exercicio(golden,muito).
exercicio(pastor,muito).
exercicio(xoloitzcuintli,_).
exercicio(pitbull,muito).

%Temperamento
temperamento(pastor,tranquilo).
temperamento(golden,tranquilo).
temperamento(collie,tranquilo).
temperamento(labrador,ativo).
temperamento(yorkshire,ativo).
temperamento(pug,tranquilo).
temperamento(husky,ativo).
temperamento(fila,tranquilo).
temperamento(lhasa,tranquilo).
temperamento(bulldog,ativo).
temperamento(chihuahua,ativo).
temperamento(akita,ativo).
temperamento(basset,tranquilo).
temperamento(beagle,ativo).
temperamento(dachshund,ativo).
temperamento(poodle,ativo).
temperamento(pinscher,ativo).
temperamento(rottweiler,ativo).
temperamento(bernardo,tranquilo).
temperamento(chow,tranquilo).
temperamento(xoloitzcuintli,tranquilo).
temperamento(pitbull,ativo).

%Pêlo
pelo(pastor,longo).
pelo(golden,longo).
pelo(collie,longo).
pelo(labrador,curto).
pelo(yorkshire,longo).
pelo(pug,curto).
pelo(husky,curto).
pelo(fila,curto).
pelo(lhasa,longo).
pelo(bulldog,curto).
pelo(chihuahua,longo).
pelo(akita,curto).
pelo(basset,curto).
pelo(beagle,curto).
pelo(dachshund,curto).
pelo(poodle,longo).
pelo(pinscher,curto).
pelo(rottweiler,curto).
pelo(bernardo,longo).
pelo(chow,longo).
pelo(xoloitzcuintli,curto).
pelo(pitbull,curto).

%Função
funcao(pastor,guarda).
funcao(golden,companhia).
funcao(collie,companhia).
funcao(labrador,companhia).
funcao(yorkshire,companhia).
funcao(pug,companhia).
funcao(husky,companhia).
funcao(fila,guarda).
funcao(lhasa,companhia).
funcao(bulldog,companhia).
funcao(chihuahua,companhia).
funcao(akita,_).
funcao(basset,companhia).
funcao(beagle,companhia).
funcao(dachshund,companhia).
funcao(poodle,_).
funcao(pinscher,companhia).
funcao(rottweiler,guarda).
funcao(bernardo,companhia).
funcao(chow,_).
funcao(xoloitzcuintli,_).
funcao(pitbull,_).

%Sozinho
sozinho(pastor,casa_cheia).
sozinho(golden,_).
sozinho(collie,_).
sozinho(labrador,casa_cheia).
sozinho(yorkshire,casa_cheia).
sozinho(pug,casa_cheia).
sozinho(husky,casa_cheia).
sozinho(fila,casa_cheia).
sozinho(lhasa,casa_cheia).
sozinho(bulldog,casa_cheia).
sozinho(chihuahua,_).
sozinho(akita,casa_cheia).
sozinho(basset,casa_cheia).
sozinho(beagle,casa_cheia).
sozinho(dachshund,casa_cheia).
sozinho(poodle,casa_cheia).
sozinho(pinscher,_).
sozinho(rottweiler,casa_cheia).
sozinho(bernardo,casa_cheia).
sozinho(chow,_).
sozinho(xoloitzcuintli,casa_cheia).
sozinho(pitbull,casa_cheia).

%Presença de outros animais
outros_animais(pastor,sem_outros).
outros_animais(golden,_).
outros_animais(collie,_).
outros_animais(labrador,_).
outros_animais(yorkshire,_).
outros_animais(pug,_).
outros_animais(husky,_).
outros_animais(fila,_).
outros_animais(lhasa,_).
outros_animais(bulldog,_).
outros_animais(chihuahua,sem_outros).
outros_animais(akita,_).
outros_animais(basset,_).
outros_animais(beagle,_).
outros_animais(dachshund,_).
outros_animais(poodle,_).
outros_animais(pinscher,sem_outros).
outros_animais(rottweiler,sem_outros).
outros_animais(bernardo,sem_outros).
outros_animais(chow,sem_outros).
outros_animais(xoloitzcuintli,_).
outros_animais(pitbull,sem_outros).

%Clima
clima(pastor,_).
clima(golden,_).
clima(collie,_).
clima(labrador,_).
clima(yorkshire,calor).
clima(pug,frio).
clima(husky,frio).
clima(fila,calor).
clima(lhasa,_).
clima(bulldog,frio).
clima(chihuahua,calor).
clima(akita,frio).
clima(basset,_).
clima(beagle,_).
clima(dachshund,_).
clima(poodle,_).
clima(pinscher,calor).
clima(rottweiler,_).
clima(bernardo,frio).
clima(chow,frio).
clima(xoloitzcuintli,calor).
clima(pitbull,_).

%Preço
preco(pastor,2000).
preco(golden,1200).
preco(collie,2200).
preco(labrador,1400).
preco(yorkshire,800).
preco(pug,1500).
preco(husky,1200).
preco(fila,1500).
preco(lhasa,800).
preco(bulldog,3000).
preco(chihuahua,1000).
preco(akita,2000).
preco(basset,1800).
preco(beagle,1000).
preco(dachshund,1200).
preco(poodle,3000).
preco(pinscher,1800).
preco(rottweiler,2000).
preco(bernardo,2000).
preco(chow,2500).
preco(xoloitzcuintli,1000).
preco(pitbull,1500).

valor_entre(DOG,VALOR):-preco(DOG,PREC),PREC<VALOR. 

validar_exercicio(DOG,QTD_EXER):-espaco(DOG,ESP),ESP = casa;QTD_EXER = muito.

%Consulta
raca_ideal(X,[ESP,EXER,TEMP,PELO,FUNC,SOZ,VALOR,ANI,CLI]):-
	espaco(X,ESP),
	validar_exercicio(X,EXER),
	temperamento(X,TEMP),
	pelo(X,PELO),
	funcao(X,FUNC),
	sozinho(X,SOZ),
	valor_entre(X,VALOR),
	outros_animais(X,ANI),
	clima(X,CLI),!
.

raca_ideal(X,[ESP,EXER,_,PELO,FUNC,SOZ,VALOR,ANI,CLI]):-
	espaco(X,ESP),
	validar_exercicio(X,EXER),
	temperamento(X,_),/*Segunda alternativa ignora esse fato*/
	pelo(X,PELO), 
	funcao(X,FUNC),
	sozinho(X,SOZ), 
	valor_entre(X,VALOR),
	outros_animais(X,ANI),
	clima(X,CLI),!
.

raca_ideal(X,[ESP,EXER,_,_,FUNC,SOZ,VALOR,ANI,CLI]):-
	espaco(X,ESP),
	validar_exercicio(X,EXER), 
	temperamento(X,_), 
	pelo(X,_), /*Terceira alternativa ignora mais esse fato*/
	funcao(X,FUNC),
	sozinho(X,SOZ), 
	valor_entre(X,VALOR),
	outros_animais(X,ANI),
	clima(X,CLI),!
.

raca_ideal(X,[ESP,EXER,_,_,FUNC,_,VALOR,ANI,CLI]):-
	espaco(X,ESP),
	validar_exercicio(X,EXER), 
	temperamento(X,_), 
	pelo(X,_), 
	funcao(X,FUNC),
	sozinho(X,_), /*Quarta alternativa ignora mais esse fato*/
	valor_entre(X,VALOR),
	outros_animais(X,ANI),
	clima(X,CLI),!
.

raca_ideal(X,[ESP,EXER,_,_,FUNC,_,_,ANI,CLI]):-
	espaco(X,ESP),
	validar_exercicio(X,EXER), 
	temperamento(X,_), 
	pelo(X,_), 
	funcao(X,FUNC),
	sozinho(X,_), 
	valor_entre(X,3000), /*Quinta alternativa aumenta o range desse fato*/
	outros_animais(X,ANI),
	clima(X,CLI),!
.

raca_ideal(X,[ESP,EXER,_,_,FUNC,_,_,ANI,CLI]):-
	espaco(X,ESP),
	validar_exercicio(X,EXER), 
	temperamento(X,_), 
	pelo(X,_), 
	funcao(X,FUNC),
	sozinho(X,_), 
	valor_entre(X,1000000), /*Sexta alternativa aumenta mais o range desse fato*/
	outros_animais(X,ANI),
	clima(X,CLI)
.

info_cao(X,ESP,EXER,TEMP,PELO,FUNC,SOZ,PREC,ANI,CLI):-
	espaco(X,ESP),
	exercicio(X,EXER),
	temperamento(X,TEMP),
	pelo(X,PELO),
	funcao(X,FUNC),
	sozinho(X,SOZ),
	preco(X,PREC),
	outros_animais(X,ANI),
	clima(X,CLI)
.